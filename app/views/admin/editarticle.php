<script src="http://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<h2>Update Article</h2>
<?php

if (isset($postErrors)){
    echo "<div style='color: red;border: 1px solid red;padding: 5px 10px;margin: 5px;'>";
    foreach ($postErrors as $key => $value){
        switch ($key){
            case 'title':
                foreach ($value as $val){
                    echo "Title ".$val."<br>";
                }
                break;
            case 'content':
                foreach ($value as $val){
                    echo "Content ".$val."<br>";
                }
                break;
            case 'cat':
                foreach ($value as $val){
                    echo "Category ".$val."<br>";
                }
                break;
            default:
                break;
        }
    }
    echo "</div>";
}

?>

<?php
   foreach ($postById as $key => $value){

?>
<form action="<?php echo BASE_URL;?>/Admin/updatePost/<?php echo $value['id']?>" method="post">
    <table>
        <tr>
            <td>Article Title</td>
            <td><input type="text" name="title" value="<?php echo $value['title']?>"></td>
        </tr>

        <tr>
            <td>Article Content</td>
            <td>
                <textarea name="content">
                    <?php echo $value['content']?>
                </textarea>
                <script>
                    CKEDITOR.replace( 'content' );
                </script>
            </td>

        </tr>

        <tr>
            <td>Article Category</td>
            <td>
                <select name="cat" class="select">
                    <option>Select One---</option>
                    <?php
                    foreach ($catlist as $key => $cat){

                        ?>
                        <option
                            <?php
                            if ($value['cat'] == $cat['id']){ ?>
                                selected = "selected"
                            <?php } ?>
                            value ="<?php echo $cat['id']?>"><?php echo $cat['name']?>
                        </option>
                    <?php }?>
                </select>
            </td>
        </tr>

        <tr>
            <td></td>
            <td><input type="submit" name="submit" value="Update Article"></td>
        </tr>
    </table>
</form>
<?php }?>