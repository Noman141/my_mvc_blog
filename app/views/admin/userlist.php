<h2>User List</h2>
<?php
if (!empty($_GET['msg'])){
    $msg = unserialize(urldecode($_GET['msg']));
    foreach ($msg as $key => $value){
        echo "<span style='color:blue;font-weight:bold'>".$value."</span>";
    }
}
?>
<table class="tblone">
    <tr>
        <th>Serial</th>
        <th>User Name</th>
        <th>User Role</th>
        <th>Action</th>
    </tr>
    <?php
    $i = 0;
    foreach ($alluser as $key => $value){
        $i++;
        ?>
        <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $value['username'];?></td>
            <td>
                <?php
                   if ($value['label'] == 1){
                       echo "Admin";
                   }elseif ($value['label'] == 2){
                       echo "Author";
                   }elseif ($value['label'] == 3){
                       echo "Editor";
                   }elseif ($value['label'] == 4){
                       echo "Contributor";
                   }

                ?>
            </td>
            <td>
                <a onclick="return confirm('Are You Sure To Delete!')" href="<?php echo BASE_URL;?>/User/DeleteUser/<?php echo $value['id'];?>">Delete</a>
            </td>

        </tr>
    <?php }?>
</table>
