<h3>Add User</h3>

<?php

if (isset($postErrors)){
    echo "<div style='color: red;border: 1px solid red;padding: 5px 10px;margin: 5px;'>";
    foreach ($postErrors as $key => $value){
        switch ($key){
            case 'username':
                foreach ($value as $val){
                    echo "User Name ".$val."<br>";
                }
                break;
            case 'password':
                foreach ($value as $val){
                    echo "Password ".$val."<br>";
                }
                break;
            case 'label':
                foreach ($value as $val){
                    echo "User Role ".$val."<br>";
                }
                break;
            default:
                break;
        }
    }
    echo "</div>";
}
?>

<form action="<?php echo BASE_URL;?>/User/AddNewUser" method="post">
    <table>
        <tr>
            <td>User Name</td>
            <td><input type="text" name="username"></td>
        </tr>

        <tr>
            <td>Password</td>
            <td><input type="text" name="password"></td>
        </tr>

        <tr>
            <td>User Role</td>
            <td>
                <select name="label" class="select">
                    <option >Select User Role---</option>
                    <option value="1">Admin</option>
                    <option value="2">Author</option>
                    <option value="3">Editor</option>
                    <option value="4">Contributor</option>
                </select>
            </td>
        </tr>

        <tr>
            <td></td>
            <td><input type="submit" name="submit" value="Save"></td>
        </tr>
    </table>
</form>