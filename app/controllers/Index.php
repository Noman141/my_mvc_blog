<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/13/2019
 * Time: 9:27 PM
 */

class Index extends NController {
    public function __construct(){
        parent::__construct();
    }

    public function Index(){
        $this->home();
    }

    public function home(){
        $tableUi = "tbl_ui";
        $UiModel = $this->load->model('UiModel');
        $data['colorname'] = $UiModel->getColor($tableUi);
        $this->load->view('header',$data);
        $data = array();
        $tablePost = "post";
        $tableCat = 'category';
        //$this->load->view('headhome',$data);

        $catModel = $this->load->model('CatModel');
        $data['catlist'] = $catModel->catList($tableCat);
        $this->load->view('search',$data);


        $postModel = $this->load->model("PostModel");
        $data['allPost'] =$postModel->getAllPost($tablePost);
        $this->load->view('content',$data);

        $data['latestPost'] =$postModel->getLatestPost($tablePost);


        $this->load->view('sidebar',$data);
        $this->load->view('footer');
    }

    public function postDetails($id=NULL){
        $data = array();
        $tablePost = "post";
        $tableCat = "category";

        $this->load->view('header');
        $catModel = $this->load->model('CatModel');
        $data['catlist'] = $catModel->catList($tableCat);
        $this->load->view('search',$data);


        $postModel = $this->load->model("PostModel");
        $data['postById'] =$postModel->getPostById($tablePost,$tableCat,$id);

        $this->load->view('details',$data);

        //sidebar code
        $data['latestPost'] =$postModel->getLatestPost($tablePost);
        //sidebar code

        $this->load->view('sidebar',$data);
        $this->load->view('footer');
    }

    public function postByCat($id = NULL){
        $data = array();
        $tablePost = "post";
        $tableCat = "category";
        $this->load->view('header');
        $catModel = $this->load->model('CatModel');
        $data['catlist'] = $catModel->catList($tableCat);
        $this->load->view('search',$data);

        $postModel = $this->load->model("PostModel");
        $data['postByCat'] =$postModel->postByCat($tablePost,$tableCat,$id);

        $this->load->view('postbycat',$data);
        //sidebar code

        $data['latestPost'] =$postModel->getLatestPost($tablePost);
        //sidebar code
        $this->load->view('sidebar',$data);
        $this->load->view('footer');

    }

    public function search(){
        $data = array();
        $keyword = $_REQUEST['keyword'];
        $cat = $_REQUEST['cat'];

        $tablePost = "post";
        $tableCat = "category";
        $this->load->view('header');
        $catModel = $this->load->model('CatModel');
        $data['catlist'] = $catModel->catList($tableCat);
        $this->load->view('search',$data);

        $postModel = $this->load->model("PostModel");
        $data['postBySearch'] =$postModel->getPostBySearch($tablePost,$keyword,$cat);

        $this->load->view('searchResult',$data);
        //sidebar code

        $data['latestPost'] =$postModel->getLatestPost($tablePost);
        //sidebar code
        $this->load->view('sidebar',$data);
        $this->load->view('footer');
    }

    public function notFound(){
        $this->load->view('404');

    }
}