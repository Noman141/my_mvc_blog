<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/15/2019
 * Time: 11:13 AM
 */

class Category extends NController {
    public function __construct(){
        parent::__construct();
    }

    public function categoryList(){

        $data = array();
        $table = 'category';
        $catModel = $this->load->model('CatModel');
        $data['cat'] = $catModel->catList($table);
        $this->load->view('category',$data);
    }

    public function catById(){
        $data = array();
        $table = 'category';
        $id = 6;
        $catModel = $this->load->model('CatModel');
        $data['catbyid'] = $catModel->catById($table,$id);
        $this->load->view('catbyid',$data);
    }

    public function addCategory(){
        $this->load->view('addCategory');
    }

    public function insertCategory(){
        $table = 'category';

        $name =  $_POST['name'];
        $title = $_POST['title'];

        $data = array(
            'name' => $name,
            'title' => $title
        );
        $catModel =$this->load->model('CatModel');
        $result = $catModel->insertCat($table,$data);

        $mdata = array();
        if ($result == 1){
            $mdata['msg'] = "Category added successfully";
        }else{
            $mdata['msg'] = "Category Not added";
        }

        $this->load->view('addCategory', $mdata);
    }


    public function updateCategory(){
        $data = array();
        $table = 'category';
        $id = 10;
        $catModel = $this->load->model('CatModel');
        $data['catById'] = $catModel->catById($table,$id);
        $this->load->view('updateCategory',$data);;
    }

    public function updateCat(){
        $table = 'category';
        $id =  $_POST['id'];
        $name =  $_POST['name'];
        $title = $_POST['title'];
        $cond = "id=$id";
        $data = array(
            'name' => $name,
            'title' => $title
        );
        $catModel = $this->load->model("CatModel");
        $result = $catModel->catUpdate($table,$data,$cond);

        $mdata = array();
        if ($result == 1){
            $mdata['msg'] = "Category Updated successfully";

        }else{
            $mdata['msg'] = "Category Not Updated";
        }
        $this->load->view('updateCategory', $mdata);
    }

    public function deleteCatById(){
        $table = 'category';
        $cond = "id=13";
        $catModel = $this->load->model("CatModel");
        $catModel->deleteCatById($table,$cond);
    }
}