<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/18/2019
 * Time: 12:42 AM
 */
Session::checkSession();
if (Session::get("label") == 1){

    class User extends NController{
        public function __construct(){
            parent::__construct();
        }

        public function Index(){
            $this->makeUser();
        }

        public function makeUser(){
    //             $tableUser = 'tbl_user';
            $this->load->view('admin/header');
            $this->load->view('admin/sidebar');

            $this->load->view('admin/adduser');
            $this->load->view('admin/footer');
        }

        public function AddNewUser(){
            if (!($_POST)) {
                header("Location: " . BASE_URL . "/User");
            }
            $input = $this->load->validation("NForm");
            $input->post('username')
                ->isEmpty();


            $input->post('password')
                ->isEmpty();

            $input->post('label')
                ->isCatEmpty();

            if ($input->submit()) {
                $tableUser = "tbl_user";
                $username = $input->values['username'];
                $password = md5($input->values['password']);
                $label = $input->values['label'];

                $data = array(
                    'username' => $username,
                    'password' => $password,
                    'label' => $label
                );
                $UserModel = $this->load->model('UserModel');
                $result = $UserModel->insertUser($tableUser, $data);

                $mdata = array();
                if ($result == 1) {
                    $mdata['msg'] = "New User added successfully";
                } else {
                    $mdata['msg'] = "User Not added!";
                }

                $url = BASE_URL . "/User/userList?msg=" . urlencode(serialize($mdata));
                header("Location:$url");
            } else {
                $data['postErrors'] = $input->errors;

                $tableUser = "tbl_user";
                $this->load->view('admin/header');
                $this->load->view('admin/sidebar');

                $UserModel = $this->load->model('UserModel');
                $data['user'] = $UserModel->getUserList($tableUser);

                $this->load->view('admin/adduser', $data);
                $this->load->view('admin/footer');
            }
        }

        public function userList(){
            $this->load->view('admin/header');
            $this->load->view('admin/sidebar');

            $data = array();
            $tableUser = 'tbl_user';
            $UserModel = $this->load->model('UserModel');
            $data['alluser'] = $UserModel->getUserList($tableUser);

            $this->load->view('admin/userlist', $data);
            $this->load->view('admin/footer');
        }

        public function DeleteUser($id = NULL){
            $tableUser = 'tbl_user';
            $cond = "id=$id";
            $UserModel = $this->load->model("UserModel");
            $result = $UserModel->deleteUserById($tableUser, $cond);

            $mdata = array();
            if ($result == 1) {
                $mdata['msg'] = "User Deleted successfully";

            } else {
                $mdata['msg'] = "User Not Deleted";
            }
            $url = BASE_URL . "//User/userList?msg=" . urlencode(serialize($mdata));
            header("Location:$url");
        }
    }
}