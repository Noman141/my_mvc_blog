<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/16/2019
 * Time: 8:26 PM
 */

class Admin extends NController {
    public function __construct(){
        parent::__construct();
        Session::checkSession();
    }

    public function Index(){
        $this->home();
    }

    public function home(){
        $tableUi = "tbl_ui";
        $UiModel = $this->load->model('UiModel');
        $data['colorname'] = $UiModel->getColor($tableUi);
        $this->load->view('admin/header',$data);
        $this->load->view('admin/sidebar');
        $this->load->view('admin/home');

        $this->load->view('admin/footer');
    }

    public function addCategory(){
        $this->load->view('admin/header');
        $this->load->view('admin/sidebar');
        $this->load->view('admin/addCategory');
        $this->load->view('admin/footer');
    }

    public function categoryList(){
        $this->load->view('admin/header');
        $this->load->view('admin/sidebar');

        $data = array();
        $table = 'category';
        $catModel = $this->load->model('CatModel');
        $data['cat'] = $catModel->catList($table);
        $this->load->view('admin/categoryList',$data);

        $this->load->view('admin/footer');
    }

    public function insertCategory(){
        $table = 'category';

        $name =  $_POST['name'];
        $title = $_POST['title'];

        $data = array(
            'name' => $name,
            'title' => $title
        );
        $catModel =$this->load->model('CatModel');
        $result = $catModel->insertCat($table,$data);

        $mdata = array();
        if ($result == 1){
            $mdata['msg'] = "Category added successfully";
        }else{
            $mdata['msg'] = "Category Not added";
        }

        $url = BASE_URL."/Admin/categoryList?msg=".urlencode(serialize($mdata));
        header("Location:$url");
    }

    public function editCategory($id = NULL){
        $data = array();
        $table = 'category';
        $this->load->view('admin/header');
        $this->load->view('admin/sidebar');

        $catModel = $this->load->model('CatModel');
        $data['updateCatId'] = $catModel->catById($table,$id);
        $this->load->view('admin/updateCategory',$data);;

        $this->load->view('admin/footer');
    }

    public function updateCat($id = NULL){
        $table = 'category';
        $name =  $_POST['name'];
        $title = $_POST['title'];
        $cond = "id=$id";
        $data = array(
            'name' => $name,
            'title' => $title
        );
        $catModel = $this->load->model("CatModel");
        $result = $catModel->catUpdate($table,$data,$cond);

        $mdata = array();
        if ($result == 1){
            $mdata['msg'] = "Category Updated successfully";

        }else{
            $mdata['msg'] = "Category Not Updated";
        }
        $url = BASE_URL."/Admin/categoryList?msg=".urlencode(serialize($mdata));
        header("Location:$url");
    }

    public function deleteCategory($id = NULL){
        $table = 'category';
        $cond = "id=$id";
        $catModel = $this->load->model("CatModel");
        $result = $catModel->deleteCatById($table,$cond);

        $mdata = array();
        if ($result == 1){
            $mdata['msg'] = "Category Deleted successfully";

        }else{
            $mdata['msg'] = "Category Not Deleted";
        }
        $url = BASE_URL."/Admin/categoryList?msg=".urlencode(serialize($mdata));
        header("Location:$url");
    }

    public function addArticle(){
        $data = array();
        $tableCat = 'category';
        $this->load->view('admin/header');
        $this->load->view('admin/sidebar');

        $catModel = $this->load->model('CatModel');
        $data['catlist'] = $catModel->catList($tableCat);

        $this->load->view('admin/addArticle',$data);
        $this->load->view('admin/footer');
    }

    public function addNewArticle(){
        if (!($_POST)){
            header("Location: ".BASE_URL."/Admin/addArticle");
        }
        $input = $this->load->validation("NForm");
        $input->post('title')
              ->isEmpty()
              ->length(10,500);

        $input->post('content')
            ->isEmpty();

        $input->post('cat')
            ->isCatEmpty();

        if ($input->submit()){
            $tablePost = "post";
            $title = $input->values['title'];
            $content = $input->values['content'];
            $cat = $input->values['cat'];

            $data = array(
                'title' => $title,
                'content' => $content,
                'cat' => $cat
            );
            $postModel = $this->load->model('PostModel');
            $result = $postModel->insertPost($tablePost, $data);

            $mdata = array();
            if ($result == 1) {
                $mdata['msg'] = "New Article added successfully";
            } else {
                $mdata['msg'] = "Article Not added!";
            }

            $url = BASE_URL."/Admin/articleList?msg=".urlencode(serialize($mdata));
            header("Location:$url");
        }else{
            $data['postErrors'] = $input->errors;

            $tableCat = 'category';
            $this->load->view('admin/header');
            $this->load->view('admin/sidebar');

            $catModel = $this->load->model('CatModel');
            $data['catlist'] = $catModel->catList($tableCat);

            $this->load->view('admin/addArticle',$data);
            $this->load->view('admin/footer');
        }
    }

    public function articleList($id = NULL){
        $data = array();
        $tablePost = "post";
        $tableCat = 'category';

        $this->load->view('admin/header');
        $this->load->view('admin/sidebar');

        $postModel = $this->load->model("PostModel");
        $data['listPost'] =$postModel->getPostList($tablePost);

        $catModel = $this->load->model('CatModel');
        $data['listCat'] = $catModel->catList($tableCat);

        $this->load->view('admin/articlelist',$data);
        $this->load->view('admin/footer');
    }

    public function editArticle($id = NULL){
        $data = array();
        $tablePost = "post";
        $tableCat = 'category';

        $this->load->view('admin/header');
        $this->load->view('admin/sidebar');

        $postModel = $this->load->model("PostModel");
        $data['postById'] =$postModel->postById($tablePost,$id);

        $catModel = $this->load->model('CatModel');
        $data['catlist'] = $catModel->catList($tableCat);


        $this->load->view('admin/editarticle',$data);
        $this->load->view('admin/footer');
    }

    public function updatePost($id = NULL){
        if (!($_POST)) {
            header("Location: " . BASE_URL . "/Admin/addArticle");
        }
        $input = $this->load->validation("NForm");
        $input->post('title')
            ->isEmpty()
            ->length(10, 500);

        $input->post('content')
            ->isEmpty();

        $input->post('cat')
            ->isCatEmpty();

        $cond = "id=$id";

        if ($input->submit()) {
            $tablePost = "post";
            $title = $input->values['title'];
            $content = $input->values['content'];
            $cat = $input->values['cat'];

            $data = array(
                'title' => $title,
                'content' => $content,
                'cat' => $cat
            );
            $postModel = $this->load->model('PostModel');
            $result = $postModel->updatePost($tablePost, $data, $cond);


            $mdata = array();
            if ($result == 1) {
                $mdata['msg'] = "Article Updated successfully";

            } else {
                $mdata['msg'] = "Article Not Updated";
            }
            $url = BASE_URL . "/Admin/articleList?msg=" . urlencode(serialize($mdata));
            header("Location:$url");
        }else{
            $tablePost = 'post';
            $tableCat = 'category';
            $data['postErrors'] = $input->errors;
            $this->load->view('admin/header');
            $this->load->view('admin/sidebar');

            $postModel = $this->load->model("PostModel");
            $data['postById'] =$postModel->postById($tablePost,$id);

            $catModel = $this->load->model('CatModel');
            $data['catlist'] = $catModel->catList($tableCat);

            $this->load->view('admin/editArticle',$data);
            $this->load->view('admin/footer');
        }
    }

    public function deleteArticle($id = NULL){
        $tablePost = 'post';
        $cond = "id=$id";
        $PostModel = $this->load->model("PostModel");
        $result  = $PostModel->deletePostById($tablePost,$cond);

        $mdata = array();
        if ($result == 1){
            $mdata['msg'] = "Article Deleted successfully";

        }else{
            $mdata['msg'] = "Article Not Deleted";
        }
        $url = BASE_URL."/Admin/articleList?msg=".urlencode(serialize($mdata));
        header("Location:$url");
    }

    public function uiOption(){
        $tableUi = "tbl_ui";
        $UiModel = $this->load->model('UiModel');
        $data['colorname'] = $UiModel->getColor($tableUi);

        $this->load->view('admin/header',$data);
        $this->load->view('admin/sidebar');

        $this->load->view('admin/ui');
        $this->load->view('admin/footer');
    }

    public function changeUI(){
        if (!($_POST)) {
            header("Location: " . BASE_URL . "/Admin/addArticle");
        }
        $id = 1;
        $input = $this->load->validation("NForm");
        $input->post('colorname');
        $cond = "id=$id";

        $tableUi = "tbl_ui";
        $colorname = $input->values['colorname'];

        $data = array(
            'colorname' => $colorname
        );
        $UiModel = $this->load->model('UiModel');
        $result = $UiModel->updateUi($tableUi, $data, $cond);


            $mdata = array();
            if ($result == 1) {
                $mdata['msg'] = "Background Color Updated successfully";

            } else {
                $mdata['msg'] = "Background Color Not Updated";
            }
            $url = BASE_URL . "/Admin/uiOption?msg=" . urlencode(serialize($mdata));
            header("Location:$url");

        /*
        else{
            $tablePost = 'post';
            $tableCat = 'category';
            $data['postErrors'] = $input->errors;
            $this->load->view('admin/header');
            $this->load->view('admin/sidebar');

            $postModel = $this->load->model("PostModel");
            $data['postById'] =$postModel->postById($tablePost,$id);

            $catModel = $this->load->model('CatModel');
            $data['catlist'] = $catModel->catList($tableCat);

            $this->load->view('admin/editArticle',$data);
            $this->load->view('admin/footer');
        }
        */
    }


}