<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/16/2019
 * Time: 8:59 PM
 */

class LoginModel extends NModel {
     public function __construct(){
         parent::__construct();
     }

     public function userControll($table,$username,$password){
         $sql = "SELECT * FROM $table WHERE username =? AND password =?";
         return $this->db->affectedRows($sql,$username,$password);
     }

     public function getUserData($table,$username,$password){
         $sql = "SELECT * FROM $table WHERE username =? AND password =?";
         return $this->db->selectUser($sql,$username,$password);
     }
}