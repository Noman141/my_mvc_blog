<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/15/2019
 * Time: 9:35 PM
 */

class PostModel extends NModel{
     public function __construct(){
         parent::__construct();
     }

     public function getAllPost($table){
         $sql = "SELECT * FROM $table ORDER BY id DESC LIMIT 3";
         return $this->db->select($sql);
     }

    public function postById($tablePost,$id){
        $sql = "SELECT * FROM $tablePost WHERE id=$id";
        return $this->db->select($sql);
    }

     public function getPostList($tablePost){
         $sql = "SELECT * FROM $tablePost ORDER BY id DESC";
         return $this->db->select($sql);
     }

     public function getPostById($tablePost,$tableCat,$id){
         $sql = "SELECT $tablePost.*,$tableCat.name FROM $tablePost
                 INNER JOIN $tableCat
                 ON $tablePost.cat = $tableCat.id
                 WHERE $tablePost.id = $id";
         return $this->db->select($sql);
     }

     public function postByCat($tablePost,$tableCat,$id){
         $sql = "SELECT $tablePost.*,$tableCat.name FROM $tablePost
                 INNER JOIN $tableCat
                 ON $tablePost.cat = $tableCat.id
                 WHERE $tableCat.id = $id";
         return $this->db->select($sql);
     }

     public function getLatestPost($table){
         $sql = "SELECT * FROM $table ORDER BY id DESC LIMIT 5";
         return $this->db->select($sql);
     }

     public function getPostBySearch($tablePost,$keyword,$cat){
         if (empty($keyword) && $cat == 0){ ?>
             "<script> location.replace("<?php echo BASE_URL;?>/Index/home"); </script>"
         <?php }
         if (isset($keyword) && !empty($keyword)){
             $sql = "SELECT * FROM $tablePost WHERE title LIKE '%$keyword%' OR content LIKE '%$keyword%'";
         }elseif (isset($cat)){
             $sql = "SELECT * FROM $tablePost WHERE cat = $cat";
         }

         return $this->db->select($sql);
     }

     public function insertPost($tablePost,$data){
         return $this->db->insert($tablePost,$data);
     }

     public function updatePost($tablePost, $data,$cond){
         return $this->db->update($tablePost,$data,$cond);
     }

     public function deletePostById($tablePost,$cond){
         return $this->db->delete($tablePost,$cond);
     }
}