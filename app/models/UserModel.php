<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/18/2019
 * Time: 12:50 AM
 */

class UserModel extends NModel {
    public function __construct(){
        parent::__construct();
    }

    public function getUserList($tableUser){
        $sql = "SELECT * FROM $tableUser";
        return $this->db->select($sql);
    }

    public function insertUser($tableUser, $data){
        return $this->db->insert($tableUser,$data);
    }

    public function deleteUserById($tableUser,$cond){
        return $this->db->delete($tableUser,$cond);
    }
}