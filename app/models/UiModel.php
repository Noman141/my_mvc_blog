<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/18/2019
 * Time: 2:21 PM
 */

class UiModel extends NModel {
     public function __construct(){
         parent::__construct();
     }

     public function getColor($tableUi){
         $sql = "SELECT * FROM $tableUi ";
         return $this->db->select($sql);
     }

     public function updateUi($tableUi, $data, $cond){
         return $this->db->update($tableUi,$data,$cond);
     }
}