<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/13/2019
 * Time: 9:40 PM
 */

class Load{
   public function __construct(){
   }

   public function view($fileName, $data = false ){
       if ($data == true){
           extract($data);
       }
       include "app/views/".$fileName.".php";

   }

   public function model($modelName){
       include "app/models/".$modelName.".php";
       return new $modelName();

   }

    public function validation($modelName){
        include "app/validation/".$modelName.".php";
        return new $modelName();

    }
}