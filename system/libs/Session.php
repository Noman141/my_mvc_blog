<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 1/16/2019
 * Time: 9:16 PM
 */

class Session{
    public static function init(){
        Session_start();
    }

    public static function set($key,$val){
        $_SESSION[$key] = $val;
    }

    public static function get($key){
        if (isset($_SESSION[$key])){
          return $_SESSION[$key];
        }else{
            return false;
        }
    }

    public static function checkSession(){
        self::init();
        if (self::get("login") == false){
            self::destroy();
            header("Location: ".BASE_URL."/Login");
        }
    }

    public static function destroy(){
        Session_destroy();
    }
}